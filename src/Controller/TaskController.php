<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Tasklist;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/app/task", name="app.task.")
 */
class TaskController extends AbstractController
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/{id<\d+>}/delete", name="delete")
     */
    public function task_delete($id): Response
    {
        $task = $this->em->getRepository(Task::class)->find($id);


        // check if task exists
        if ($task == null) {
            return $this->redirectToRoute('app.index');
        }

        // check if task belongs to user
        if ($task->getTasklist()->getOwner() != $this->getUser()) {
            return  $this->redirectToRoute('app.index');
        }

        $listid = $task->getTasklist()->getId();

        $this->em->remove($task);
        $this->em->flush();
        return $this->redirectToRoute('app.list', ['id' => $listid]);
    }


    /**
     * @Route("/{id<\d+>}/complete", name="complete")
     */
    public function task_complete($id): Response
    {
        $task = $this->em->getRepository(Task::class)->find($id);


        // check if task exists
        if ($task == null) {
            return $this->redirectToRoute('app.index');
        }

        // check if task belongs to user
        if ($task->getTasklist()->getOwner() != $this->getUser()) {
            return  $this->redirectToRoute('app.index');
        }

        $listid = $task->getTasklist()->getId();

        $task->complete();

        $this->em->persist($task);
        $this->em->flush();
        return $this->redirectToRoute('app.list', ['id' => $listid]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function task_create(Request $request): Response
    {

        // get last url and extract list 
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        $lastPath = substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));

        $r = $this->get('router')->match($lastPath);
        $list = $this->em->getRepository(Tasklist::class)->find($r['id']);



        // check if list belongs to user or for empty input
        if ($list->getOwner() != $this->getUser() || empty($request->get('newTask'))) {
            return $this->redirectToRoute('app.list', ['id' => $r['id']]);
        }

        // create new task
        $task = new Task();
        $task->setTitle($request->get('newTask'));
        $task->setTasklist($list);

        // save
        $this->em->persist($task);
        $this->em->flush();

        return $this->redirectToRoute('app.list', ['id' => $r['id']]);
    }
}
