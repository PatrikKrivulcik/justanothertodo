<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Tasklist;
use App\Repository\TasklistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/app", name="app.")
 */
class AppController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    public $em;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        // get user tasks and calculate statistics
        $lists = $this->getUser()->getTasklists()->toArray();
        $due = [];
        $completed = [];

        $avgTimeToComplete = 0; // minutes
        $avgTimeToCompleteByList = [];

        foreach ($lists as $list) {
            foreach ($list->getTasks() as $task) {
                // sort into due and completed
                if ($task->getCompleted() == null) {
                    $due[]  = $task;
                } else {
                    $completed[] = $task;

                    // calculate time taken
                    $interval = $task->getCompleted()->getTimestamp() -  $task->getCreated()->getTimestamp();
                    $interval /= 60;
                    $avgTimeToComplete  += $interval;

                    if (isset($avgTimeToComplete[$list->getTitle()])) {
                        $avgTimeToCompleteByList[$list->getTitle()] += $interval;
                    } else {
                        $avgTimeToCompleteByList[$list->getTitle()] =  $interval;
                    }
                }
            }
            // calculate average for list
            $count = count($list->getTasks()->filter(function(Task $item){
                return $item->getCompleted() != null;
            }));
            $avgTimeToCompleteByList[$list->getTitle()] /=  $count;
            $avgTimeToCompleteByList[$list->getTitle()] =  round($avgTimeToCompleteByList[$list->getTitle()], 1);

        }

        // calculate average time to complete and change to readable format
        $avgTimeToComplete /= count($completed);
        $avgTimeToComplete = (int) round($avgTimeToComplete, 0);

        if($avgTimeToComplete < 60){
            $avgTimeToComplete.= ' minutes';
        }else if($avgTimeToComplete < 60*24){
            $avgTimeToComplete = round($avgTimeToComplete, 2);
            $avgTimeToComplete .= ' hours';
        }else{
            $avgTimeToComplete = round($avgTimeToComplete/ ( 60*24), 2);
            $avgTimeToComplete .= ' days';
        }





        // map due to readable array
        $due = array_map(function (Task $task) {
            return [
                'id' => $task->getId(),
                'title' => $task->getTitle(),
                'created' => $task->getCreated(),
                'list' => $task->getTasklist()->getTitle()
            ];
        }, $due);



        return $this->render('app/index.html.twig', [
            'lists' => $this->getUserTaskLists(),
            'due' => $due,
            'avgTimeToCompleteByList'=> $avgTimeToCompleteByList,
            'avgTimeToComplete' => $avgTimeToComplete
        ]);
    }


    /**
     * @Route("/list/{id<\d+>}", name="list")
     */
    public function list($id): Response
    {
        // fetch list
        $list = $this->em->getRepository(Tasklist::class)->find($id);

        // check if list belongs to user
        if ($list == null || $list->getOwner() != $this->getUser()) {
            return $this->redirectToRoute('app.index');
        }

        $listname = $list->getTitle();

        // get tasks
        $alltasks = $list->getTasks()->toArray();

        // divide into groups
        $tasks = [];
        $completed = [];
        foreach ($alltasks as $task) {
            if ($task->getCompleted() === null) {
                $tasks[] = $task;
            } else {
                $completed[] = $task;
            }
        }

        // map entities to arrays
        $completed = array_map(function ($i) {
            return [
                'id' => $i->getId(),
                'title' => $i->getTitle(),
                'completed' => $i->getCompleted(),
                'created' => $i->getCreated()
            ];
        }, $completed);

        $tasks = array_map(function ($i) {
            return [
                'id' => $i->getId(),
                'title' => $i->getTitle(),
                'created' => $i->getCreated()
            ];
        }, $tasks);

        return $this->render('app/list.html.twig', [
            'lists' => $this->getUserTaskLists(),
            'completed' => $completed,
            'tasks' => $tasks,
            'listname' => $listname
        ]);
    }


    /**
     * @Route("/list", name="list.redirect")
     */
    public function list_redirect(): Response
    {
        return $this->redirectToRoute('app.index');
    }

    /**
     * @Route("/list/create", name="list.create")
     */
    public function list_create(Request $request): Response
    {
        // handle submission
        if ($request->get('newList') != null) {
            $newName = $request->get('newList');

            // create new list
            $list = new Tasklist();
            $list->setTitle($newName);
            $list->setOwner($this->getUser());

            // save
            $this->em->persist($list);
            $this->em->flush();
        }
        return $this->render(
            'app/list.create.html.twig',
            [
                'lists' => $this->getUserTaskLists()
            ]
        );
    }

    public function getUserTaskLists()
    {
        $lists = $this->getUser()->getTasklists()->toArray();
        $lists = array_map(function (Tasklist $i) {
            return [
                'id' => $i->getId(),
                'title' => $i->getTitle()
            ];
        }, $lists);
        return $lists;
    }
}
