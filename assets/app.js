import Chart from 'chart.js';
import './styles/app.scss';



let colors = [
    '#DE67BA',
    '#FF71A0',
    '#FF8A83',
    '#FFAD6B',
    '#FFD361',
    '#F9F871',
]


let avgTimeToCompleteByList = document.getElementById('avgTimeToCompleteByList')

let avgTimeToCompleteByListData = { Nice: 5, Good: 25 * 60 }
// let avgTimeToCompleteByListData = JSON.parse(avgTimeToCompleteByList.dataset.data)
let avgTimeToCompleteByListDataColors = []
for (let i = 0; i < Math.ceil(Object.keys(avgTimeToCompleteByListData).length / colors.length); i++) {
    avgTimeToCompleteByListDataColors = avgTimeToCompleteByListDataColors.concat(colors)
}
console.log(avgTimeToCompleteByListDataColors)
let avgTimeToCompleteByListChart = new Chart(avgTimeToCompleteByList, {
    data: {
        labels: Object.keys(avgTimeToCompleteByListData),
        datasets: [{
            label: '# of Votes',
            data: Object.values(avgTimeToCompleteByListData),
            backgroundColor: avgTimeToCompleteByListDataColors
        }]
    },
    type: 'doughnut',
    options: {
        title: {
            display: true,
            text: "Average Time to Compmlete by List"
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {

                    const mins = data.datasets[0].data[tooltipItem.index]
                    if (mins < 60) {
                        return data.labels[tooltipItem.index] + ' - ' + mins + ' minutes'
                    } else if (mins < 60 * 24) {
                        return data.labels[tooltipItem.index] + ' - ' + (mins / 60).toFixed(2) + ' hours'
                    } else {
                        return data.labels[tooltipItem.index] + ' - ' + (mins / (60 * 24)).toFixed(2) + ' days'
                    }
                }
            }
        }
    }
}
)

